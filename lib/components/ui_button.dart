import 'package:baseprovider/components/ui_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class UIButton extends StatelessWidget {
  final VoidCallback onPressed;
  final UIText text;
  final double? width;
  final double? height;
  final ButtonStyle? style;
  final Widget? icon;

  const UIButton(
      {Key? key,
      required this.text,
      required this.onPressed,
      this.width,
      this.height,
      this.style,
      this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: ElevatedButton(
        onPressed: onPressed,
        style: style!.merge(
          ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.r),
            ),
          ),
        ),
        child: icon != null
            ? Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  icon!,
                  text,
                ],
              )
            : text,
      ),
    );
  }
}
