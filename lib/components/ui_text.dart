
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class UIText extends StatelessWidget {
  final String text;
  final TextStyle? style;
  final TextAlign? textAlign;
  final FontWeight fontWeight;
  final double? fontSize;
  final int? maxLines;

  const UIText.bold({
    Key? key,
    required this.text,
    this.style,
    this.textAlign,
    this.fontWeight = FontWeight.w700,
    this.fontSize,
    this.maxLines = 5,
  }) : super(key: key);

  const UIText.medium( {
    Key? key,
    required this.text,
    this.style,
    this.fontWeight = FontWeight.w600,
    this.textAlign,
    this.fontSize,
    this.maxLines = 5,
  }) : super(key: key);

  const UIText.regular({
    Key? key,
    required this.text,
    this.style,
    this.fontWeight = FontWeight.w400,
    this.textAlign,
    this.fontSize,
    this.maxLines = 5,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontWeight: fontWeight,
        color: Colors.black,
        fontSize: fontSize ?? 14.sp,
      ).merge(style),
      textAlign: textAlign,
      maxLines: maxLines,
      overflow: TextOverflow.ellipsis,
    );
  }
}
