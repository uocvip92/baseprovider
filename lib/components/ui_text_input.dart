import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../resource_shared/ui_colors.dart';
import '../utils/log_utils.dart';

class UITextInput extends StatefulWidget {
  final String? hint;
  final String? errorText;
  final bool? validate;
  final bool obscureText;
  final AutovalidateMode autoValidateMode;
  final double? textInputWidth;
  final double? textInputHeight;
  final TextStyle? textStyle;
  final TextStyle? errorTextStyle;
  final TextEditingController textEditingController;
  final FocusNode? currentFocusNode;
  final FocusNode? nextFocusNode;
  final TextInputType? textInputType;
  final double? hintSize;
  final Color? hintColor;
  final Color borderColor;
  final Color enabledBorderColor;
  final Color disabledBorderColor;
  final Widget? suffixIcon;
  final bool? isInputReadOnly;
  final bool? enable;
  final int? maxLines;
  final TextInputAction? textInputAction;
  final double? endPadding;
  final ValueChanged<String>? onChange;
  final Iterable<String>? autofillHints;
  final VoidCallback? onEditingComplete;
  final TextCapitalization textCapitalization;

  const UITextInput({
    Key? key,
    this.hint,
    required this.textEditingController,
    this.textInputType = TextInputType.text,
    this.currentFocusNode,
    this.nextFocusNode,
    this.hintSize,
    this.hintColor = Colors.grey,
    this.borderColor = Colors.grey,
    this.enabledBorderColor = Colors.grey,
    this.disabledBorderColor = Colors.grey,
    this.suffixIcon,
    this.textStyle,
    this.textInputWidth = double.infinity,
    this.textInputHeight,
    this.validate,
    this.errorText,
    this.obscureText = false,
    this.errorTextStyle,
    this.isInputReadOnly,
    this.autoValidateMode = AutovalidateMode.disabled,
    this.enable = true,
    this.maxLines = 1,
    this.textInputAction = TextInputAction.next,
    this.endPadding,
    this.onChange,
    this.autofillHints,
    this.onEditingComplete,
    this.textCapitalization = TextCapitalization.none,
  }) : super(key: key);

  @override
  _UITextInputState createState() => _UITextInputState();
}

class _UITextInputState extends State<UITextInput> {
  bool _isShow = false;
  bool _obscureText = false;

  @override
  void initState() {
    super.initState();
    _obscureText = widget.obscureText;
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      readOnly: widget.isInputReadOnly ?? false ? true : false,
      controller: widget.textEditingController,
      keyboardType: widget.textInputType,
      textCapitalization: widget.textCapitalization,
      obscureText: _obscureText,
      textInputAction: widget.textInputAction,
      enabled: widget.enable,
      onEditingComplete: widget.onEditingComplete,
      focusNode: widget.currentFocusNode,
      maxLines: widget.maxLines,
      onFieldSubmitted: (_) {
        if (widget.currentFocusNode != null) {
          widget.currentFocusNode!.unfocus();
          FocusScope.of(context).requestFocus(widget.nextFocusNode);
        }
      },
      autofillHints: widget.autofillHints,
      decoration: InputDecoration(
        contentPadding: widget.endPadding == null
            ? EdgeInsets.symmetric(horizontal: 12.w)
            : EdgeInsets.only(left: 12.w, right: widget.endPadding!),
        fillColor: Colors.grey.withOpacity(0.2),
        filled: true,
        hintText: widget.hint,
        hintStyle: TextStyle(
          fontSize: widget.hintSize ?? 14.sp,
          color: widget.hintColor,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8.r),
          borderSide: BorderSide(
            style: BorderStyle.none,
            color: widget.borderColor,
          ),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8.r),
          borderSide: BorderSide(
            style: BorderStyle.none,
            color: widget.enabledBorderColor,
          ),
        ),
        disabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8.r),
          borderSide: BorderSide(
            style: BorderStyle.none,
            color: widget.disabledBorderColor,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8.r),
          borderSide: BorderSide(
            style: BorderStyle.none,
            color: widget.isInputReadOnly ?? false ? Colors.grey : Colors.blue,
          ),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.red),
          borderRadius: BorderRadius.circular(8.r),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.red),
          borderRadius: BorderRadius.circular(8.r),
        ),
        errorStyle: widget.errorTextStyle ?? TextStyle(fontSize: 13.sp),
        suffixIcon: widget.suffixIcon ??
            (widget.obscureText
                ? GestureDetector(
                    onTap: () {
                      setState(() {
                        _isShow = !_isShow;
                        _obscureText = !_isShow;
                        LogUtils.i('suffixIcon onTap $_obscureText');
                      });
                    },
                    child: Icon(
                      _isShow ? Icons.visibility : Icons.visibility_off,
                      color: UIColors.primaryColor,
                    ),
                  )
                : null),
      ),
      onChanged: widget.onChange,
      style: widget.textStyle,
      autovalidateMode: widget.autoValidateMode,
      validator: (String? value) {
        if ((widget.validate != null && widget.validate!)) {
          return widget.errorText;
        } else {
          return null;
        }
      },
    );
  }
}
