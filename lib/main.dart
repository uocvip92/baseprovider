import 'package:baseprovider/resource_shared/languages/localizations.dart';
import 'package:baseprovider/resource_shared/ui_colors.dart';
import 'package:baseprovider/resource_shared/ui_constants.dart';
import 'package:baseprovider/services/api/network/network_injection.dart';
import 'package:baseprovider/services/navigation/navigation_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'models/route_observe_model.dart';

final RouteObserver<Route<dynamic>> routeObserver =
    RouteObserverModel.topRouteObserver();

void main() async {
  await initNetwork();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      builder: (_, __) => MaterialApp(
        builder: (context, widget) {
          ScreenUtil.init(context);
          return MediaQuery(
            //Setting font does not change with system font size
            data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
            child: widget!,
          );
        },
        theme: ThemeData(
          primaryColor: UIColors.primaryColor,
          backgroundColor: Colors.white,
        ),
        onGenerateRoute: NavigationServices.instance.routeBuilders,
        navigatorObservers: [routeObserver],
        localizationsDelegates: AppLocalizations.localizationsDelegates,
        supportedLocales: AppLocalizations.supportedLocales,
      ),
      designSize: const Size(UIConstants.designWidth, UIConstants.designHeight),
    );
  }
}
