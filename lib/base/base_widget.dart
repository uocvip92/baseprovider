import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';

import '../resource_shared/ui_colors.dart';
import '../utils/utils.dart';
import 'base_view_model.dart';

class BaseWidget<VM extends BaseViewModel> extends StatelessWidget {
  final VM viewModel;
  final Widget Function(BuildContext context) child;

  const BaseWidget({Key? key, required this.viewModel, required this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<VM>(
      create: (_) => viewModel,
      builder: (BuildContext context, _) {
        return Stack(
          children: [
            GestureDetector(
              onTap: () {
                Utils.hideKeyboard(context);
              },
              child: child(context),
            ),
            Visibility(
              visible: context.select<VM, bool>((vm) => vm.isLoading),
              child: Positioned.fill(
                child: Container(
                  alignment: Alignment.center,
                  color: Colors.black.withOpacity(0.2),
                  child: SpinKitCircle(
                    color: UIColors.primaryColor,
                  ),
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
