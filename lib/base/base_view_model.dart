import 'package:flutter/material.dart';

import '../utils/log_utils.dart';
import 'network_aware_state.dart';

enum ViewState {
  success,
  busy,
  idle,
}

class BaseViewModel with ChangeNotifier, NetworkAwareState {
  ViewState _viewState = ViewState.idle;

  ViewState get viewState => _viewState;

  bool _isConnected = false;

  bool get isConnected => _isConnected;

  bool isLoading = false;
  bool _isDisposed = false;

  void showLoading() {
    isLoading = true;
    setState(ViewState.success);
  }

  void hideLoading() {
    isLoading = false;
    setState(ViewState.success);
  }

  late BuildContext _context;

  BuildContext get context => _context;

  void onInit(BuildContext context) {
    _context = context;
    LogUtils.i('Init ViewModel');
    initConnectivity();
  }

  @override
  void onDisconnected() {
    LogUtils.i('Network onDisconnected');
    _isConnected = false;
  }

  @override
  void onConnected() {
    LogUtils.i('Network onReconnected');
    _isConnected = true;
  }

  void setState(ViewState viewState) {
    if (_viewState == viewState && viewState != ViewState.success ||
        _isDisposed) return;
    LogUtils.i('setState $viewState');
    _viewState = viewState;
    notifyListeners();
  }

  @override
  void dispose() {
    LogUtils.i('onClose all');
    _isDisposed = true;
    cancelSubscription();
    super.dispose();
  }
}
