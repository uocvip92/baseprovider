import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';

import '../utils/log_utils.dart';

mixin NetworkAwareState {
  bool _isDisconnected = false;
  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> _networkSubscription;

  void onConnected();

  void onDisconnected();

  Future<void> initConnectivity() async {
    ConnectivityResult result = await getNetworkStatus();
    _updateConnectionStatus(result);
    _networkSubscription =
        _connectivity.onConnectivityChanged.listen((_result) {
      _updateConnectionStatus(_result);
    });
  }

  Future<ConnectivityResult> getNetworkStatus() async {
    ConnectivityResult result = ConnectivityResult.none;
    try {
      result = await _connectivity.checkConnectivity();
    } on Exception catch (e) {
      LogUtils.e(e.toString());
    }
    return result;
  }

  _updateConnectionStatus(ConnectivityResult result) {
    if (result != ConnectivityResult.none) {
      if (!_isDisconnected) {
        onConnected();
        _isDisconnected = true;
      }
    } else {
      _isDisconnected = false;
      onDisconnected();
    }
  }

  void cancelSubscription() {
    try {
      _networkSubscription.cancel();
    } catch (e) {
      LogUtils.e(e.toString());
    }
  }
}
