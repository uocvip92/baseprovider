import 'package:baseprovider/services/navigation/route_constants.dart';
import 'package:flutter/material.dart';

import '../../views/home/home_view.dart';
import '../../views/signin/signin_view.dart';
import 'custome_page_route.dart';

class NavigationServices {
  static const NavigationServices _instance = NavigationServices._internal();

  static NavigationServices get instance => _instance;

  const NavigationServices._internal();

  factory NavigationServices() {
    return _instance;
  }

  Route<dynamic> routeBuilders(RouteSettings settings) {
    switch (settings.name) {
      case RouteConstants.signInRoute:
        return MaterialPageRoute(
            builder: (_) => const SignInView(),
            settings: const RouteSettings(name: RouteConstants.signInRoute));

      case RouteConstants.homeRoute:
        final arguments = settings.arguments as Map<String, dynamic>;
        return CustomPageRoute(
          HomeView(
            email: arguments['email'],
            password: arguments['password'],
          ),
          settings: const RouteSettings(name: RouteConstants.homeRoute),
        );

      default:
        return MaterialPageRoute(
            builder: (_) => const SignInView(),
            settings: const RouteSettings(name: RouteConstants.signInRoute));
    }
  }

  void navigateToSignInScreen(BuildContext context) {
    Navigator.of(context).pushNamedAndRemoveUntil(
        RouteConstants.signInRoute, (Route<dynamic> route) => false);
  }

  void navigateToHomeScreenNotRemove(
      BuildContext context, String email, String password) {
    Navigator.pushNamed(context, RouteConstants.homeRoute,
        arguments: {'email': email, 'password': password});
  }

  void navigateToHomeScreen(BuildContext context) {
    Navigator.of(context).pushNamedAndRemoveUntil(
      RouteConstants.homeRoute,
      (Route<dynamic> route) => false,
    );
  }
}
