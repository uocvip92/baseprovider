import 'package:baseprovider/models/param/signin_param.dart';
import 'package:dio/dio.dart';

import 'api_constants.dart';
import 'network/request_callback.dart';
import 'network/request_factory.dart';
import 'network/request_method.dart';

class UserService {
  final Dio _dio;

  UserService(
    this._dio,
  );

  Future<void> signIn(SignInParam param, BaseRequestCallback callback) async {
    await BaseRequestFactory()
        .addBaseUrl(APIConstants.domain)
        .addEndPointUrl(APIConstants.signIn)
        .addRequestMethod(BaseRequestMethod.get)
        .addCallback(callback)
        .addParams(param.toJson())
        .doRequest(_dio);
  }
}
