import 'dart:io';

import 'package:baseprovider/extensions/string_extension.dart';
import 'package:dio/dio.dart';

import '../../../models/response/error_response_model.dart';

abstract class NetworkExceptions {
  static const tag = "NetworkExceptions";

  static Map<String, String?> handleResponse(Map<String, dynamic> json) {
    List<Error>? errorListModel = ErrorResponseModel.fromJson(json).errors;
    if (errorListModel != null && errorListModel.isNotEmpty) {
      Error errorModel = errorListModel.first;
      String? strErrorCode = errorModel.code;
      String? message = errorModel.message;
      String? param = errorModel.param;
      if (param != null) {
        message = '${param.inCaps} ${errorModel.message}';
      }
      return {'code': strErrorCode, 'message': message};
    } else {
      return {
        'code': null,
        'message': 'Could not connect the server, please try again later.'
      };
    }
  }

  static Map<String, String?> getDioException(error) {
    late String networkExceptions =
        'Could not connect the server, please try again later.';
    dynamic result;
    if (error is Exception) {
      try {
        if (error is DioError) {
          switch (error.type) {
            case DioErrorType.cancel:
              networkExceptions = "Request Cancelled";
              break;
            case DioErrorType.receiveTimeout:
              networkExceptions = "Connection request timeout";
              break;
            case DioErrorType.unknown:
              networkExceptions = "No internet connection";
              break;
            case DioErrorType.badResponse:

              /// [TODO] check logic ??
              result = handleResponse(error.response!.data);
              break;
            case DioErrorType.sendTimeout:
              networkExceptions = "Send timeout in connection with API server";
              break;
            default:
              networkExceptions = "Unexpected error occurred";
              break;
          }
        } else if (error is SocketException) {
          networkExceptions = "No internet connection";
        } else {
          networkExceptions = "Unexpected error occurred";
        }
        result ??= {'code': null, 'message': networkExceptions};
        return result;
      } on FormatException catch (_) {
        networkExceptions = "Unexpected error occurred";
      } catch (_) {
        networkExceptions = "Unexpected error occurred";
      }
    } else {
      if (error.toString().contains("is not a subtype of")) {
        networkExceptions = "Unable to process the data";
      } else {
        networkExceptions = "Unexpected error occurred";
      }
    }
    print(tag + "dioRequest()=>error=" + networkExceptions);
    result ??= {'code': null, 'message': networkExceptions};
    return result;
  }
}
