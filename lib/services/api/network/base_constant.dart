class BaseConstant {
  static const int statusCodeSuccess = 200;
  static const int statusCodeCreateSuccess = 201;
  static const int statusCodeError = 400;
  static const int timeout = 15000;
}
