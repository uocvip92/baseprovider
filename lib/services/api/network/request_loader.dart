import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:baseprovider/services/api/network/request_callback.dart';
import 'package:baseprovider/services/api/network/request_exceptions.dart';
import 'package:baseprovider/services/api/network/request_method.dart';
import 'package:dio/dio.dart';
import 'package:dio/io.dart';

import 'base_constant.dart';

class BaseRequestLoader<T> {
  static const tag = "BaseRequestLoader";

  late String _baseUrl;
  String _newBaseUrl = '';
  late String _endPointUrl;
  late int _requestMethod;
  late BaseRequestCallback<T> _callback;
  Map<String, String> _headers = {};
  late Map<String, dynamic> _params;
  late bool _isAuthRequest;
  late bool _isContentTypeApplicationJson;
  String _authToken = '';
  String _userToken = '';
  int _timeout = BaseConstant.timeout;

  BaseRequestLoader<T> addBaseUrl(String baseUrl) {
    _baseUrl = baseUrl;
    return this;
  }

  BaseRequestLoader<T> changeBaseUrl(String newBaseUrl) {
    _newBaseUrl = newBaseUrl;
    return this;
  }

  BaseRequestLoader<T> addRequestUrl(String endPointUrl) {
    _endPointUrl = endPointUrl;
    return this;
  }

  BaseRequestLoader<T> addRequestMethod(int requestType) {
    _requestMethod = requestType;
    return this;
  }

  BaseRequestLoader<T> addHeaders(Map<String, String> header) {
    _headers = header;
    return this;
  }

  BaseRequestLoader<T> addParams(Map<String, dynamic> params) {
    _params = params;
    return this;
  }

  BaseRequestLoader<T> addCallback(BaseRequestCallback<T> callback) {
    _callback = callback;
    return this;
  }

  BaseRequestLoader<T> isAuthRequest(bool isAuthRequest) {
    _isAuthRequest = isAuthRequest;
    return this;
  }

  BaseRequestLoader<T> isContentTypeApplicationJsonRequest(
      bool isContentTypeApplicationJson) {
    _isContentTypeApplicationJson = isContentTypeApplicationJson;
    return this;
  }

  BaseRequestLoader<T> addAuthToken(String token) {
    _authToken = token;
    return this;
  }

  BaseRequestLoader<T> setTimeout(int timeout) {
    _timeout = timeout;
    return this;
  }

  BaseRequestLoader<T> addUserToken(String token) {
    _userToken = token;
    return this;
  }

  // request with dio lib
  Future dioRequest(Dio dio) async {
    if (_endPointUrl.isEmpty || _baseUrl.isEmpty) {
      throw Exception('URL is empty!!');
    }
    if (_callback.onStart != null) {
      _callback.onStart!();
    }

    try {
      if (_newBaseUrl.isNotEmpty) {
        _baseUrl = _newBaseUrl;
      }
      if (_isContentTypeApplicationJson) {
        _headers["content-type"] = "application/json";
      }
      if (_isAuthRequest && _authToken.isNotEmpty) {
        _headers["X-Client"] = _authToken;
      }

      if (_isAuthRequest && _userToken.isNotEmpty) {
        _headers["authorization"] = "Bearer $_userToken";
      }

      // certificate always return true
      dio.httpClientAdapter = IOHttpClientAdapter(
        createHttpClient: () {
          final client = HttpClient();
          // Config the client.
          client.findProxy = (uri) {
            // Forward all request to proxy "localhost:8888".
            // Be aware, the proxy should went through you running device,
            // not the host platform.
            return '';
          };
          client.badCertificateCallback =
              (X509Certificate cert, String host, int port) {
            return true;
          };
          // You can also create a new HttpClient for Dio instead of returning,
          // but a client must being returned here.
          return client;
        },
      );

      // set options
      dio.options.baseUrl = _baseUrl;
      if (_headers.isNotEmpty) {
        dio.options.headers = _headers;
      }
      dio.options.connectTimeout = Duration(seconds: _timeout);
      dio.options.receiveTimeout = Duration(seconds: _timeout);

      Response response;
      FormData? formData;
      if (_params.isNotEmpty) {
        formData = FormData.fromMap(_params);
      }

      switch (_requestMethod) {
        case BaseRequestMethod.postList:
          response = await dio.post(_endPointUrl,
              data: jsonEncode(_params),
              onSendProgress: _callback.onSendProgress);
          break;
        case BaseRequestMethod.post:
          response = await dio.post(_endPointUrl,
              data: formData, onSendProgress: _callback.onSendProgress);
          break;
        case BaseRequestMethod.get:
          response = await dio.get(_endPointUrl,
              queryParameters: _params.isEmpty ? null : _params);
          break;
        case BaseRequestMethod.put:
          response = await dio.put(_endPointUrl,
              data: json.encode(_params),
              onSendProgress: _callback.onSendProgress);
          break;
        case BaseRequestMethod.delete:
          response = await dio.delete(_endPointUrl, data: formData);
          break;
        default:
          response = await dio.get(_endPointUrl, queryParameters: _params);
      }

      int? statusCode = response.statusCode;
      String jsonResponse = response.data.toString();
      print(_endPointUrl +
          " =>statusCode= $statusCode \n params=$_params \n json=$jsonResponse");
      if (statusCode! < BaseConstant.statusCodeSuccess ||
          statusCode >= BaseConstant.statusCodeError) {
        _callback.onError(NetworkExceptions.handleResponse(response.data));
      } else {
        _callback.onCompleted(response.data);
      }
    } catch (error) {
      print(_endPointUrl + " =>error=" + error.toString());
      _callback.onError(
        NetworkExceptions.getDioException(error),
      );
    }
  }
}
