class BaseRequestCallback<T> {
  BaseRequestCallback(
      {this.onStart,
      required this.onCompleted,
      required this.onError,
      this.onSendProgress});

  Function? onStart;

  Function(T) onCompleted;

  Function(dynamic e) onError;

  Function(int sent, int total)? onSendProgress;
}
