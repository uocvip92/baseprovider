
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';

import '../../../repositories/user_repository.dart';
import '../user_services.dart';

final GetIt getIt = GetIt.instance;

Future<void> initNetwork() async {
  getIt.registerLazySingleton<Dio>(() => Dio());

  getIt.registerLazySingleton<UserService>(
      () => UserService(getIt<Dio>()));
  getIt.registerLazySingleton<IUserRepository>(
      () => UserRepository(getIt<UserService>()));


}
