class BaseRequestMethod {
  static const int postList = 0;
  static const int post = 1;
  static const int get = 2;
  static const int put = 3;
  static const int delete = 4;
}
