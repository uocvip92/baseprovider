import 'dart:async';

import 'package:baseprovider/services/api/network/request_callback.dart';
import 'package:baseprovider/services/api/network/request_loader.dart';
import 'package:dio/dio.dart';

import 'base_constant.dart';

class BaseRequestFactory<T> {
  String _baseUrl = '';
  String _endPointUrl = '';
  late int _requestMethod;
  late BaseRequestCallback<T> _callback;
  Map<String, String> _headers = {};
  Map<String, dynamic> _params = {};
  bool _isAuthRequest = true;
  bool _isContentTypeApplicationJson = true;
  String _authToken = '';
  String _userToken = '';
  int _timeout = BaseConstant.timeout;

  BaseRequestFactory<T> addBaseUrl(String baseUrl) {
    _baseUrl = baseUrl;
    return this;
  }

  BaseRequestFactory<T> addEndPointUrl(String url) {
    _endPointUrl = url;
    return this;
  }

  BaseRequestFactory<T> addRequestMethod(int requestMethod) {
    _requestMethod = requestMethod;
    return this;
  }

  BaseRequestFactory<T> addHeaders(Map<String, String> header) {
    _headers = header;
    return this;
  }

  BaseRequestFactory<T> addParams(Map<String, dynamic> params) {
    _params = params;
    return this;
  }

  BaseRequestFactory<T> addCallback(BaseRequestCallback<T> callback) {
    _callback = callback;
    return this;
  }

  BaseRequestFactory<T> isAuthRequest(bool isAuthRequest) {
    _isAuthRequest = isAuthRequest;
    return this;
  }

  BaseRequestFactory<T> isContentTypeApplicationJsonRequest(
      bool isContentTypeApplicationJson) {
    _isContentTypeApplicationJson = isContentTypeApplicationJson;
    return this;
  }

  BaseRequestFactory<T> addAuthToken(String token) {
    _authToken = token;
    return this;
  }

  BaseRequestFactory<T> setTimeout(int timeout) {
    _timeout = timeout;
    return this;
  }

  BaseRequestFactory<T> addUserToken(String token) {
    _userToken = token;
    return this;
  }

  Future doRequest(Dio dio) {
    var requestLoader = BaseRequestLoader<T>();
    requestLoader.addRequestUrl(_endPointUrl);
    requestLoader.addRequestMethod(_requestMethod);
    requestLoader.isAuthRequest(_isAuthRequest);
    requestLoader
        .isContentTypeApplicationJsonRequest(_isContentTypeApplicationJson);
    requestLoader.setTimeout(_timeout);

    if (_baseUrl.isNotEmpty) requestLoader.addBaseUrl(_baseUrl);
    requestLoader.addHeaders(_headers);
    requestLoader.addParams(_params);
    requestLoader.addCallback(_callback);
    if (_authToken.isNotEmpty) requestLoader.addAuthToken(_authToken);
    if (_userToken.isNotEmpty) requestLoader.addUserToken(_userToken);
    return requestLoader.dioRequest(dio);
  }
}
