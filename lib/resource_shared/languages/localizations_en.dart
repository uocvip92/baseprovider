import 'localizations.dart';

/// The translations for English (`en`).
class AppLocalizationsEn extends AppLocalizations {
  AppLocalizationsEn([String locale = 'en']) : super(locale);

  @override
  String get appName => 'CAGT';

  @override
  String get txtEmailHint => 'Email';

  @override
  String get txtEmailInvalid => 'Email Invalid';

  @override
  String get txtPasswordHint => 'Password';

  @override
  String get txtPasswordInvalid => 'Password Invalid';

  @override
  String get txtSignIn => 'SignIn';
}
