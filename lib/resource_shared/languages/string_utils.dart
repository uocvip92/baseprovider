
import 'package:flutter/material.dart';

import 'localizations.dart';

class StringUtils {
  late final BuildContext context;

  StringUtils(this.context);

  static bool isEmpty(String? s) {
    if (s == null) return true;
    if (s.isEmpty) return true;
    return false;
  }

  String get appName => AppLocalizations.of(context)!.appName;
  String get txtEmailHint => AppLocalizations.of(context)!.txtEmailHint;
  String get txtEmailInvalid => AppLocalizations.of(context)!.txtEmailInvalid;
  String get txtPasswordHint => AppLocalizations.of(context)!.txtPasswordHint;
  String get txtPasswordInvalid => AppLocalizations.of(context)!.txtPasswordInvalid;
  String get txtSignIn => AppLocalizations.of(context)!.txtSignIn;

}
