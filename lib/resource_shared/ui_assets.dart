class UIAssets {
  // Images assets
  static const String rice = "assets/svg/rice.svg";
  static const String rice2 = "assets/svg/rice_2.svg";
  static const String shirt = "assets/svg/shirt.svg";
  static const String money = "assets/svg/money.svg";
}