import 'dart:ui';

import 'package:hexcolor/hexcolor.dart';

class UIColors {
  UIColors._();

  static final Color primaryColor = HexColor("#0098DD");
}