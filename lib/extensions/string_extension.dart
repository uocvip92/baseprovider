import 'package:intl/intl.dart';

extension StringExtension on String {
  static const String dateFormat = 'yyyy-MM-dd HH:mm:ss';

  String get inCaps => '${this[0].toUpperCase()}${substring(1)}';

  String convertStringToDate({String format = dateFormat}) {
    final DateTime todayDate = parseToDate();
    final DateFormat formatter = DateFormat(format);
    return formatter.format(todayDate);
  }

  DateTime parseToDate({String format = dateFormat}) {
    DateTime dateTime;
    final DateFormat formatter = DateFormat(format);
    try {
      final DateTime dateParse = DateTime.parse(this).toLocal();
      dateTime = DateTime.parse(formatter.format(dateParse));
    } catch (e) {
      dateTime = formatter.parse(this, true);
    }
    return DateTime(dateTime.year, dateTime.month, dateTime.day, dateTime.hour,
        dateTime.minute, dateTime.second);
  }
}
