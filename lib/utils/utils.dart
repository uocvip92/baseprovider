import 'package:flutter/material.dart';

class Utils {
  static void hideKeyboard(BuildContext context) {
    var focusNodeCurrent = FocusNode();
    FocusScope.of(context).requestFocus(focusNodeCurrent);
  }

  static bool isEmailNoValid(String? email) {
    if (email!.isEmpty) return false;
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    final regExp = RegExp(pattern);
    return regExp.hasMatch(email);
  }
}
