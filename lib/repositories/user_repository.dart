import '../models/param/signin_param.dart';
import '../services/api/network/request_callback.dart';
import '../services/api/user_services.dart';

abstract class IUserRepository {

  Future<void> signIn(SignInParam param, BaseRequestCallback callback);
}

class UserRepository extends IUserRepository {
  final UserService _userService;

  UserRepository(this._userService);

  @override
  Future<void> signIn(SignInParam param, BaseRequestCallback callback) {
    return _userService.signIn(param, callback);
  }
}