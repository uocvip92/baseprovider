import 'package:flutter/material.dart';

import '../../components/ui_text.dart';

class HomeView extends StatefulWidget {
  final String email;
  final String password;

  const HomeView({Key? key, required this.email, required this.password})
      : super(key: key);

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 16,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                      child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: const UIText.bold(
                            text: "Back",
                            style: TextStyle(color: Colors.blueAccent),
                          ))),
                  const SizedBox(
                    height: 50,
                  ),
                  UIText.medium(text: "Email: ${widget.email}"),
                  const SizedBox(
                    height: 10,
                  ),
                  UIText.medium(text: "Password: ${widget.password}"),
                ],
              )),
        ));
  }
}
