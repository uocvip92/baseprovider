import 'package:baseprovider/models/param/signin_param.dart';
import 'package:baseprovider/services/api/network/request_callback.dart';
import 'package:baseprovider/services/navigation/navigation_service.dart';
import 'package:flutter/cupertino.dart';

import '../../base/base_view_model.dart';
import '../../repositories/user_repository.dart';
import '../../utils/utils.dart';

class SignInViewModel extends BaseViewModel {
  final TextEditingController emailTextController =
      TextEditingController(text: '');
  final TextEditingController passwordTextController =
      TextEditingController(text: '');
  final IUserRepository _userRepository;

  SignInViewModel(this._userRepository);

  bool _emailValidate = false;
  bool _passwordValidate = false;

  bool get emailValidate => _emailValidate;

  set emailValidate(bool emailValidate) {
    _emailValidate = emailValidate;
    setState(ViewState.success);
  }

  bool get passwordValidate => _passwordValidate;

  set passwordValidate(bool passwordValidate) {
    _passwordValidate = passwordValidate;
    setState(ViewState.success);
  }

  void signIn() {
    String email = emailTextController.text.trim();
    String password = passwordTextController.text;
    bool isEmailValidate = Utils.isEmailNoValid(email);
    bool isPasswordValidate = password.isEmpty;
    emailValidate = !isEmailValidate;
    passwordValidate = isPasswordValidate;
    if (isEmailValidate && !isPasswordValidate) {
      _signInRequest(email, password);
    }
  }

  void _signInRequest(String email, String password) {
    showLoading();
    final param = SignInParam(email, password);
    _userRepository.signIn(
        param,
        BaseRequestCallback(onCompleted: (data) {
          hideLoading();
          NavigationServices.instance
              .navigateToHomeScreenNotRemove(context, email, password);
        }, onError: (error) {
          hideLoading();
        }));
  }
}
