import 'package:baseprovider/base/base_widget.dart';
import 'package:baseprovider/repositories/user_repository.dart';
import 'package:baseprovider/resource_shared/ui_assets.dart';
import 'package:baseprovider/services/api/network/network_injection.dart';
import 'package:baseprovider/views/signin/signin_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

import '../../components/ui_button.dart';
import '../../components/ui_text.dart';
import '../../components/ui_text_input.dart';
import '../../resource_shared/languages/string_utils.dart';
import '../../resource_shared/ui_colors.dart';

class SignInView extends StatefulWidget {
  const SignInView({Key? key}) : super(key: key);

  @override
  _SignInViewState createState() => _SignInViewState();
}

class _SignInViewState extends State<SignInView> {
  late SignInViewModel _signInViewModel;

  @override
  void initState() {
    super.initState();
    _initViewModel(context);
  }

  void _initViewModel(BuildContext context) {
    _signInViewModel = SignInViewModel(getIt<IUserRepository>())
      ..onInit(context);
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<SignInViewModel>(
        viewModel: _signInViewModel,
        child: (BuildContext providerContext) {
          return Scaffold(
              backgroundColor: Colors.white,
              body: SafeArea(
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 16,
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      UIText.bold(
                        text: StringUtils(providerContext).appName,
                        fontSize: 30,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset(
                            UIAssets.rice,
                            width: 50,
                            height: 50,
                          ),
                          SvgPicture.asset(
                            UIAssets.shirt,
                            width: 50,
                            height: 50,
                          ),
                          SvgPicture.asset(
                            UIAssets.rice2,
                            width: 50,
                            height: 50,
                          ),
                          SvgPicture.asset(
                            UIAssets.money,
                            width: 50,
                            height: 50,
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 50,
                      ),
                      Selector<SignInViewModel, bool>(
                          selector: (_, viewModel) => viewModel.emailValidate,
                          shouldRebuild: (pre, next) => true,
                          builder: (_, emailValidate, child) {
                            return UITextInput(
                              borderColor: Colors.blueAccent,
                              textInputType: TextInputType.emailAddress,
                              textEditingController:
                                  _signInViewModel.emailTextController,
                              hint: StringUtils(context).txtEmailHint,
                              errorText: StringUtils(context).txtEmailInvalid,
                              autoValidateMode: AutovalidateMode.always,
                              validate: emailValidate,
                            );
                          }),
                      const SizedBox(
                        height: 20,
                      ),
                      UITextInput(
                        obscureText: true,
                        textEditingController:
                            _signInViewModel.passwordTextController,
                        hint: StringUtils(context).txtPasswordHint,
                        errorText: StringUtils(context).txtPasswordInvalid,
                        autoValidateMode: AutovalidateMode.always,
                        validate: providerContext.select<SignInViewModel, bool>(
                            (viewModel) => viewModel.passwordValidate),
                      ),
                      SizedBox(
                        height: 20.h,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 10.h),
                        child: UIButton(
                          text: UIText.regular(
                            text: StringUtils(context).txtSignIn,
                            fontSize: 17.sp,
                            style: const TextStyle(color: Colors.white),
                          ),
                          style: ButtonStyle(
                            backgroundColor: MaterialStateColor.resolveWith(
                                (states) => UIColors.primaryColor),
                          ),
                          onPressed: () {
                            _signInViewModel.signIn();
                          },
                          width: double.maxFinite,
                          height: 44.h,
                        ),
                      ),
                    ],
                  ),
                ),
              ));
        });
  }
}
