class SignInParam {
  final String email;
  final String password;

  SignInParam(this.email, this.password);

  Map<String, dynamic> toJson() => {
        "email": email,
        "password": password,
      };
}
