import 'dart:convert';

ErrorResponseModel errorResponseModelFromJson(String str) =>
    ErrorResponseModel.fromJson(json.decode(str));

String errorResponseModelToJson(ErrorResponseModel data) =>
    json.encode(data.toJson());

class ErrorResponseModel {
  ErrorResponseModel({
    this.errors,
  });

  List<Error>? errors;

  factory ErrorResponseModel.fromJson(Map<String, dynamic> json) =>
      ErrorResponseModel(
        errors: List<Error>.from(json["errors"].map((x) => Error.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "errors": List<dynamic>.from((errors ?? []).map((x) => x.toJson())),
      };
}

class Error {
  Error({
    this.param,
    this.message,
    this.code,
  });

  String? param;
  String? message;
  String? code;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        param: json["param"],
        message: json["message"],
        code: json["code"],
      );

  Map<String, dynamic> toJson() => {
        "param": param,
        "message": message,
        "code": code,
      };
}
